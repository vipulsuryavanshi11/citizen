var modulevar = angular.module("citizen_module",[]);
modulevar.config(['$interpolateProvider', function($interpolateProvider) {
  $interpolateProvider.startSymbol('{[');
  $interpolateProvider.endSymbol(']}');
}]).config([
 '$httpProvider',
 function($httpProvider) {
       $httpProvider.defaults.xsrfCookieName = 'csrftoken';
       $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
 }
]);
modulevar.controller("citizen_controller",["$scope", "$http", "$location", "fileUpload", function($scope,$http,$location,fileUpload)
	{

	$scope.all_citizen = [];
	$scope.data = {};
	$scope.sortingOrder = 'first_name';
    $scope.reverse = false;

	$scope.open_modal = function(temp) {
		$scope.single_citizen = temp;
		console.log("open_modal");
		$("#updateModal").modal();
		};

    $scope.sort_by = function(newSortingOrder) {
    	console.log(newSortingOrder);
        if ($scope.sortingOrder == newSortingOrder)
            $scope.reverse = !$scope.reverse;

        $scope.sortingOrder = newSortingOrder;

        // icon setup
        $('th i').each(function(){
            // icon reset
            $(this).removeClass().addClass('icon-sort');
        });
        if ($scope.reverse)
            $('th.'+newSortingOrder+' i').removeClass().addClass('icon-chevron-up');
        else
            $('th.'+newSortingOrder+' i').removeClass().addClass('icon-chevron-down');
    };

	$scope.uploadFile = function(first_name, type){
		console.log(first_name);
        var file = $scope.myFile;
        console.log('file is ' );
        console.dir(file);
        var uploadUrl = "/file_upload";
        fileUpload.uploadFileToUrl(file, uploadUrl,first_name, type);
    };

    $scope.uploadFilePhoto = function(first_name, type){
        console.log(first_name);
        var file = $scope.myPhoto;
        console.log('file is ' );
        console.dir(file);
        var uploadUrl = "/file_upload";
        fileUpload.uploadFileToUrl(file, uploadUrl,first_name, type);
    };

	var formdata = new FormData();
	console.log(formdata);
    $scope.getTheFiles = function ($files) {
        angular.forEach($files, function (value, key) {
        	console.log(key);
        	console.log(value);
            formdata['file_name'] = value;
       		console.log(formdata);
        });
    };
	
	$scope.add_citizen = function()	
	{	
		// alert('5555');
		console.log(formdata);
		
		$scope.data['file_name'] = formdata['file_name'];
		console.log($scope.data);
		$http.post('/',$scope.data).success(function(response)
			{
				console.log(response.redirect);
				window.location = response.redirect;
			});	
	}

	$scope.get_citizen = function()	
	{	
		
		$http.get('/get_citizen').success(function(response)
			{

				$scope.all_citizen = response.all_citizens;
				console.log($scope.all_citizen);
				//window.location = response.redirect;
			});	
	} 
	$scope.get_citizen();

	$scope.citizenDelete = function(id)	
	{	
		console.log(id);
		var config ={url: '/citizenList/delete/',
					method: 'POST',
		 			data: {'pk' : id},
	 			}
		
		$http(config).success(function(response)
			{
				$scope.all_citizen = response.all_citizens;

			});	
	}
	$scope.citizenUpdate = function(id)	
	{	
		var data = $scope.single_citizen;
		data['id'] = id;
		$http.post('/citizenList/update/',data).success(function(response)
			{
				console.log(response.redirect);

				window.location = response.redirect;
			});	
	} 

}

]).directive('fileModel', ['$parse', function ($parse) {
    return {
        restrict: 'A',
        link: function(scope, element, attrs) {
            var model = $parse(attrs.fileModel);
            var modelSetter = model.assign;
            
            element.bind('change', function(){
                scope.$apply(function(){
                    modelSetter(scope, element[0].files[0]);
                });
            });
        }
    };
}]).service('fileUpload', ['$http', function ($http) {
    this.uploadFileToUrl = function(file, uploadUrl, first_name, type){
        var fd = new FormData();
        var file_name = type + '__' + first_name;
        console.log(first_name);
        console.log(file_name);

        fd.append(file_name, file);
        $http.post(uploadUrl, fd, {
            transformRequest: angular.identity,
            headers: {'Content-Type': undefined}
        })
        .success(function(){
        })
        .error(function(){
        });
    }
}]);