from django.db import models

class Citizen(models.Model):
	first_name = models.CharField(max_length = 20)
	last_name = models.CharField(max_length = 30)
	birth_date = models.DateField(null=True)
	email = models.CharField(max_length=50, unique=True)
	mobile = models.CharField(max_length=15)
	address = models.CharField(max_length=500)
	city = models.CharField(max_length=50)
	pincode = models.CharField(max_length=6)
	qualification = models.CharField(max_length=90)
	document_type = models.CharField(max_length=50)
