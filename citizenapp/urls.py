from django.conf.urls import url
from . import views
app_name = 'citizenapp'

urlpatterns = [
	url(r'^$',views.citizenAdd, name = 'citizenAdd'),
	url(r'^citizenList/$', views.citizenList, name = 'citizenList'),
	url(r'^citizenList/delete/', views.citizenDelete, name = 'citizenDelete'),
	url(r'^get_citizen', views.get_citizen, name = 'get_citizen'),
	url(r'^file_upload', views.file_upload, name = 'file_upload'),
	url(r'^citizenList/update/', views.citizenUpdate, name = 'citizenUpdate'),

]