from .models import Citizen
from django.shortcuts import redirect,render,get_object_or_404
from django import forms
from django.core.files.storage import FileSystemStorage
import json
from django.http import JsonResponse,Http404, HttpResponse
from django.conf import settings
import os
from django.core import serializers
from datetime import datetime

'''  for Get the all citizens  '''
def get_citizen(request):
	data = {}
	citizens = Citizen.objects.all()
	data['all_citizens'] = citizens
	resp = []
	if citizens:
		for each in list(citizens):
			temp = {}
			temp['first_name'] = each.first_name
			temp['last_name'] = each.last_name
			temp['birth_date'] = datetime.strftime(each.birth_date, '%Y-%m-%d')
			temp['email'] = each.email
			temp['mobile'] = each.mobile
			temp['city'] = each.city
			temp['address'] = each.address
			temp['qualification'] = each.qualification
			temp['pk'] = each.pk
			temp['pincode'] = each.pincode
			resp.append(temp)
	data['all_citizens'] = resp
	print data
	return HttpResponse(json.dumps(data), content_type='application/json')
	# return JsonResponse(data)

def citizenList(request,template_name='list.html'):
	return render(request,template_name)


'''  for file upload '''
def file_upload(request,template_name='common_form.html'):
	if request.method == "POST":
		# print request.body
		# print request.FILES['document']
		for name, file in request.FILES.iteritems():
			current_file = os.path.join(settings.BASE_DIR, 'media', name)
			if os.path.exists(current_file): 
				os.remove(current_file)
	   		fs = FileSystemStorage()
			filename = fs.save(name, file)
			uploaded_file_url = fs.url(filename)
			# print uploaded_file_url

'''  for create citizen '''
def citizenAdd(request,template_name='common_form.html'):
	if request.method == "POST":
		print request.body
		# print request.FILES
		data = json.loads(request.body)
		birth_date = data['birth_date'].split('T')[0]
		birth_date = datetime.strptime(birth_date, '%Y-%m-%d')
		data['birth_date'] = birth_date
		print birth_date
		citizen = Citizen(**data)
		# print citizen.first_name
		# citizen = all_data
		citizen.save()
		responseData = {'message':'success','status' :200, 'redirect' :'citizenList/'}
		return JsonResponse(responseData)
	return render(request,template_name)

''' for updating the citizen record '''
def citizenUpdate(request,template_name = 'update_form.html'):

	if request.method == 'POST':
		responseData = {}
		data = json.loads(request.body)
		# print data
		data.pop('pk',None)
		birth_date = data['birth_date'].split('T')[0]
		birth_date = datetime.strptime(birth_date, '%Y-%M-%d')
		data['birth_date'] = birth_date
		p=Citizen.objects.get(pk=data['id'])
		# print p
		if p:
			# print "inside p"
			Citizen.objects.filter(pk=data['id']).update(**data)
			responseData = {'redirect' :''}
			# print responseData
		return JsonResponse(responseData)

''' for deleting the citizen record '''
def citizenDelete(request):
	data = json.loads(request.body)
	# print data['pk']
	citizen = get_object_or_404(Citizen,pk=data['pk'])
	if request.method == 'POST':
		citizen.delete()

		citizens = Citizen.objects.all()
		resp = []
		if citizens:
			for each in list(citizens):
				temp = {}
				temp['first_name'] = each.first_name
				temp['email'] = each.email
				temp['mobile'] = each.mobile
				temp['city'] = each.city
				temp['qualification'] = each.qualification
				temp['pk'] = each.pk
				resp.append(temp)
		data['all_citizens'] = resp
		return HttpResponse(json.dumps(data), content_type='application/json')
	return redirect('citizenapp:citizenList')

